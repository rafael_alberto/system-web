<%@ tag description="inputText" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="entidade" required="true" %>
<%@ attribute name="atributo" required="true" %>
<%@ attribute name="objeto" required="true"  type="br.com.systemweb.core.enumeration.GenericEnum" %> 
<%@ attribute name="lista" required="true" type="br.com.systemweb.core.enumeration.GenericEnum[]" %>
<%@ attribute name="readOnly" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="cssClassLabel" required="false" %>
<%@ attribute name="cssClassComponente" required="false" %>

<c:set var="entidadeUpper" value="<%= entidade.substring(0,1).toUpperCase() + entidade.substring(1) %>"/>
<c:set var="entidadeLower" value="<%= entidade.substring(0,1).toLowerCase() + entidade.substring(1) %>"/>
<c:set var="atributoItem" value="<%= (entidade.substring(0,1).toUpperCase() + entidade.substring(1)) %>" />

<div class="form-group">
	<label class="${cssClassLabel}" for="${atributo}">
		<spring:message code="${entidadeUpper}.${atributo}.label" /><c:if test="${required eq 'true'}"><b class="required-field"> *</b></c:if>
	</label>
		<div class="${cssClassComponente}">
			<c:choose>
				<c:when test="${readOnly}">
					<form:select path="${atributo}" class="form-control" id="${atributo}" name="${atributo}" disabled="true">
						<option value="${objeto}"><spring:message code="${objeto.descricao}"/></option>
					</form:select>
				</c:when>
				<c:otherwise>
					<c:if test="${required eq 'true'}">
						<form:select class="form-control" path="${atributo}" id="${atributo}" name="${atributo}" required="true">
							<option value=""><spring:message code="global.selecione.label"/></option>
								<c:forEach items="${lista}" var="item">
									<option ${objeto.codigo eq item.codigo ? 'selected' : ''} value="${item}">
										<spring:message code="${item.descricao}"/>
									</option>
								</c:forEach>
						</form:select>	
					</c:if>
					<c:if test="${required ne 'true'}">
						<form:select class="form-control" path="${atributo}" id="${atributo}" name="${atributo}">
							<option value=""><spring:message code="global.selecione.label"/></option>
								<c:forEach items="${lista}" var="item">
									<option ${objeto.codigo eq item.codigo ? 'selected' : ''} value="${item}">
										<spring:message code="${item.descricao}"/>
									</option>
								</c:forEach>
						</form:select>
					</c:if>
				</c:otherwise>
			</c:choose>
		</div>
</div>