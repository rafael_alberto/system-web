<%@ tag description="datepicker-simple" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="atributo" required="true" %>
<%@ attribute name="cssClassComponente" required="false" %>

<div class="input-group">
	<input type="text" class="${cssClassComponente}" data-field="${atributo}" id="${atributo}">
	<div class="input-group-addon">
    	<i class="fa fa-calendar"></i>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="${context}/layout/datepicker/daterangepicker.css" />

<script src="${context}/layout/moment/moment.min.js"></script>
<script src="${context}/layout/datepicker/daterangepicker.js"></script>
<script type="text/javascript" src="${context}/layout/javascript/system/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#${atributo}").mask("99/99/9999");		
		$("#${atributo}").daterangepicker({
	          singleDatePicker: true,
        	  format : "DD/MM/YYYY",
        	  "locale": {
        		  "daysOfWeek": [
            	  		"Dom",
            	    	"Seg",
            	    	"Ter",
            	    	"Qua",
            	        "Qui",
            	        "Sex",
            	        "S�b"
            	  ],
            	  "monthNames": [
            	        "Janeiro",
            	        "Fevereiro",
            	        "Mar�o",
            	        "Abril",
            	        "Maio",
            	        "Junho",
            	        "Julho",
            	        "Agosto",
            	        "Setembro",
            	        "Outubro",
            	        "Novembro",
            	        "Dezembro"
            	  ],
        	  }
        	  
	        }, function(start, end, label) {
	        	$("#${atributo}-error").hide();
	        });
	});
</script>