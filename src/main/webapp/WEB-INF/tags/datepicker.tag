<%@ tag description="input-text" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="entidade" required="true" %>
<%@ attribute name="atributo" required="true" %>
<%@ attribute name="required" required="true" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="cssClassLabel" required="false" %>
<%@ attribute name="cssClassComponente" required="false" %>

<c:set var="entidadeUpper" value="<%= entidade.substring(0,1).toUpperCase() + entidade.substring(1) %>"/>
<c:set var="entidadeLower" value="<%= entidade.substring(0,1).toLowerCase() + entidade.substring(1) %>"/>

<div class="form-group">
	<label class="${cssClassLabel}" for="${atributo}">
		<spring:message code="${entidadeUpper}.${atributo}.label" /><c:if test="${required eq 'true'}"><b class="required-field"> *</b></c:if>
	</label>
	<div class="${cssClassComponente}">
		<div class="input-group">
			<c:if test="${required eq 'true'}">
				<form:input type="text" class="form-control" path="${atributo}" id="${atributo}" required="true" />	
			</c:if>
			<c:if test="${required ne 'true'}">
				<form:input type="text" class="form-control" path="${atributo}" id="${atributo}" />
			</c:if>
			<div class="input-group-addon">
        		<i class="fa fa-calendar"></i>
        	</div>
        </div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="${context}/layout/datepicker/daterangepicker.css" />

<script src="${context}/layout/moment/moment.min.js"></script>
<script src="${context}/layout/datepicker/daterangepicker.js"></script>
<script type="text/javascript" src="${context}/layout/javascript/system/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#${id}").mask("99/99/9999");		
		$("#${id}").daterangepicker({
	          singleDatePicker: true,
        	  format : "DD/MM/YYYY",
        	  "locale": {
        		  "daysOfWeek": [
            	  		"Dom",
            	    	"Seg",
            	    	"Ter",
            	    	"Qua",
            	        "Qui",
            	        "Sex",
            	        "S�b"
            	  ],
            	  "monthNames": [
            	        "Janeiro",
            	        "Fevereiro",
            	        "Mar�o",
            	        "Abril",
            	        "Maio",
            	        "Junho",
            	        "Julho",
            	        "Agosto",
            	        "Setembro",
            	        "Outubro",
            	        "Novembro",
            	        "Dezembro"
            	  ],
        	  }
        	  
	        }, function(start, end, label) {
	        	$("#${id}-error").hide();
	        });
		
		$('#form${entidadeUpper}').validate({
        	rules:{
            	'${id}': {
            		dateITA: true
            	}
        	},                                                            
        	highlight: function(label) {
            	$(label).closest('.input-group').addClass('error').removeClass('success');
        	},
        	errorPlacement: function ($error, $element) {
            	div = $($element).parent().parent().closest("div");
            	$(div).append($error);
        	}
        });
	});
</script>