<%@ tag description="input-text" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="entidade" required="true" %>
<%@ attribute name="atributo" required="true" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="cssClassLabel" required="false" %>
<%@ attribute name="cssClassComponente" required="false" %>

<c:set var="entidadeUpper" value="<%= entidade.substring(0,1).toUpperCase() + entidade.substring(1) %>"/>

<div class="form-group">
	<label class="${cssClassLabel}" for="${atributo}">
		<spring:message code="${entidadeUpper}.${atributo}.label" /><c:if test="${required eq 'true'}"><b class="required-field"> *</b></c:if>
	</label>
	<div class="${cssClassComponente}">
		<c:if test="${required eq 'true'}">
			<form:input type="file" class="form-control" path="${atributo}" id="${atributo}" name="${atributo}" required="true" />
		</c:if>
		<c:if test="${required ne 'true'}">
			<form:input type="file" class="form-control" path="${atributo}" id="${atributo}" name="${atributo}" />
		</c:if>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="modalUpload">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aguarde</h4>
      </div>
      <div class="modal-body">
      		<p style="color: black;font-size: 16px;">Executando upload...</p>
      		<br>
      		<div class="overlay">	
              <i class="fa fa-refresh fa-spin"></i>
         	</div>
         	<br><br>
      </div>
    </div>
  </div>
</div>