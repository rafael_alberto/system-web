<%@ tag description="ahref" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="url" required="true" %>
<%@ attribute name="mensagem" required="true" %>
<%@ attribute name="cssClass" %>
<%@ attribute name="icone" %>

<c:choose>
	<c:when test="${icone ne null}">
		<a href="${url}" class="${cssClass}"><i class="${icone}" aria-hidden="true"></i>&nbsp;<spring:message code="${mensagem}" /></a>
	</c:when>
	<c:otherwise>
		<a href="${url}" class="${cssClass}"><spring:message code="${mensagem}" /></a>		
	</c:otherwise>
</c:choose>

