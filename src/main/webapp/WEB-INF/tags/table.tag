<%@ tag description="table" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="system" tagdir="/WEB-INF/tags" %>

<%@ attribute name="camposTabela" required="true" type="java.util.List"%>
<%@ attribute name="entidade" required="true"%>
<%@ attribute name="cssClass" required="false"%>
<%@ attribute name="esconderNovo" required="false"%>
<%@ attribute name="titulo" required="false"%>

<c:set var="entidadeUpper"
	value="<%=entidade.substring(0, 1).toUpperCase() + entidade.substring(1)%>" />
<c:set var="entidadeLower"
	value="<%=entidade.substring(0, 1).toLowerCase() + entidade.substring(1)%>" />

<section class="content-header">
	<h1 class="title">
		<c:if test="${titulo eq null}">
			<spring:message code="${entidadeUpper}.index.label" />
		</c:if>
		<c:if test="${titulo ne null}">
			<spring:message code="${titulo}" />
		</c:if>
	</h1>
	<c:if test="${esconderNovo ne 'true'}">
		<a href="${context}/${entidadeLower}/novo.do" class="btn btn-primary"
			style="float: right; margin-top: -26px;"> <i class="fa fa-file-o"></i>
			&nbsp;<spring:message code="global.novo.label" />
		</a>
	</c:if>
</section>
<section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title" style="color: gray;">
				<i class="fa fa-search"></i> &nbsp;<spring:message code="global.pesquisa.label"/>
			</h3>
			<div class="box-tools pull-right">
            	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
		</div>
		<div class="box-body">
			<div class="row">
			<c:forEach items="${camposPesquisaTabela}" var="campo">
				<c:if test="${campo.visivel eq true }">
					<div class="col-md-3">
						<label><spring:message code="${entidadeUpper}.${campo.atributo}.label"/></label>
           				<c:choose>
           					<c:when test="${campo.tipoAtributo eq 'TEXTO'}">
            					<input type="text" class="form-control custom-filter campo-pesquisa" data-field="${campo.atributo}">
            				</c:when>
							<c:when test="${campo.tipoAtributo eq 'LISTA'}">
            					<select class="form-control custom-filter campo-pesquisa" data-field="${campo.atributo}">
            						<option value=""><spring:message code="global.selecione.label"/></option>
										<c:forEach items="${campo.valoresPesquisa}" var="item">
											<option value="${item}">
												<spring:message code="${item.descricao}"/>
											</option>
										</c:forEach>
								</select>
            				</c:when>
            				<c:when test="${campo.tipoAtributo eq 'DATA'}">
	            				<system:datepicker-simple atributo="${campo.atributo}" cssClassComponente="form-control custom-filter campo-pesquisa"/>
           					</c:when>            				
           				</c:choose>
            		</div>
				</c:if>
			</c:forEach>
			</div>
		</div>
		<div class="box-footer text-center">
			<button type="button" id="btnPesquisaDataTable" class="btn btn-primary">
				<i class="fa fa-search"></i>&nbsp;<spring:message code="global.pesquisar.label"/>
			</button>
		</div>
	</div>

	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title" style="color: gray;">
				<i class="fa fa-table"></i> &nbsp;<spring:message code="global.lista.label"/>
			</h3>
		</div>
		<div class="box-body table-responsive">
			<table id="table${entidadeUpper}" class="${cssClass}">
				<thead>
					<tr>
						<c:forEach items="${camposTabela}" var="campo">
								<c:choose>
									<c:when test="${campo.tamanho ne null}">
										<th style="width: ${campo.tamanho}%;"><b><spring:message code="${entidadeUpper}.${campo.atributo}.label" /></b></th>
									</c:when>
									<c:otherwise>
										<th><b><spring:message code="${entidadeUpper}.${campo.atributo}.label" /></b></th>
									</c:otherwise>
								</c:choose>
						</c:forEach>
					</tr>
				</thead>
				<tbody />
			</table>
		</div>
	</div>

</section>

<script type="text/javascript">
	$(document).ready(function() {
		loadDataTable();
		
		$("#btnPesquisaDataTable").click(function(){
			loadDataTable();
		});
		
		shortcut.add('F2', function() {
			$("#btnPesquisaDataTable").click();
	    });
		
		shortcut.add('F4', function() {
			window.location.href = context + '/${entidadeLower}/novo.do';
	    });
	});

	jQuery.fn.exists = function () {
	 return jQuery(this).length > 0 ? true : false;
	};
	
	function loadDataTable(){
		var table = $("#table${entidadeUpper}").DataTable({
			"oLanguage": {
				"sUrl": context + "/layout/dataTable/js/dataTable-pt_br.txt"
			},
			"bDestroy": true,
			"bProcessing": true,
			"bServerSide": true,
			"bAutoWidth": false,
			"bFilter": false,
			"bLengthChange": false,
			"sAjaxSource": context + "/${entidadeLower}/loadDataAjax.do",
			"order" : [[0 , "desc"]],
	    	"aoColumns": [
				<c:forEach items="${camposTabela}" var="campo" varStatus="indice">
					<c:if test="${campo.tipoAtributo eq 'TEXTO'}">
						{ "mData": ${indice.count-1} },
					</c:if>
					<c:if test="${campo.tipoAtributo eq 'BOTAO'}">
						{ "mData": null ,
							"class": "details-control",
								"mRender": function ( o, val, data ){
									return ${campo.atributo}(context, data);
								}
						},
					</c:if>	
				</c:forEach>
			],
			"fnServerParams": function ( aoData ) {
	        	for (var i = 0; i < $(".custom-filter").length; i++) { 
	            	var field = $($(".custom-filter")[i]);
	            	var dataField = $($(".campo-pesquisa")[i]);
	            	aoData.push({ "name": "customFilter_" + dataField.attr("data-field"), "value": field.val() }); 
	            }
			}
		});
		$.fn.dataTable.ext.errMode = 'throw';
	}
	
	function editar(context, data){
		return '<div align="center"><a href="' + context + '/${entidadeLower}/editar.do?id=' + data[0] + '" class="btn btn-primary" style="width: 40px;">' +
		'<i class="fa fa-edit"></i></a></div>';
	}
	
	function excluir(context, data){
		return '<div align="center"><button type="button" class="btn btn-default" style="width: 40px;" onclick="removerRegistro(' + data[0] + ');">' +
		'<i class="fa fa-remove"></i></button></div>';
	}
	
</script>

<!-- Modal -->
<div class="modal fade" id="modalExclusao" tabindex="-1" role="dialog"
	aria-labelledby="modalExclusao">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<spring:message code="global.exclusao.label" />
				</h4>
			</div>
			<div class="modal-body">
				<p style="font-size: 16px;">
					<spring:message code="global.confirma.exclusao.label" />
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"
					style="width: 80px;">
					<spring:message code="global.nao.label" />
				</button>
				<button type="button" class="btn btn-primary" style="width: 80px;"
					onclick="confirmarExclusao();">
					<spring:message code="global.sim.label" />
				</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="idRegistroExclusao">

<script type="text/javascript">
	function removerRegistro(id){
		$("#modalExclusao").modal("show");
		$("#idRegistroExclusao").val(id);
	}
	
	function confirmarExclusao(){
		var idRegistroExclusao = $("#idRegistroExclusao").val();
		window.location.href = context + '/${entidadeLower}/excluir.do?id=' + idRegistroExclusao;
	}
</script>