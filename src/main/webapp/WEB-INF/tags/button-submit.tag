<%@ tag description="button-submit" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="mensagem" required="true" %>
<%@ attribute name="cssClass" %>
<%@ attribute name="icone" %>

<c:choose>
	<c:when test="${icone ne null}">
		<button type="submit" class="${cssClass}"><i class="${icone}" aria-hidden="true"></i>&nbsp;<spring:message code="${mensagem}" /></button>
	</c:when>
	<c:otherwise>
		<button type="submit" class="${cssClass}"><spring:message code="${mensagem}" /></button>		
	</c:otherwise>
</c:choose>

