<%@ tag description="inputText" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="entidade" required="true" %>
<%@ attribute name="atributo" required="true" %>
<%@ attribute name="readOnly" %>
<%@ attribute name="maxLength" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="cssClassLabel" required="false" %>
<%@ attribute name="cssClassComponente" required="false" %>

<c:set var="entidadeUpper" value="<%= entidade.substring(0,1).toUpperCase() + entidade.substring(1) %>"/>
<c:set var="entidadeLower" value="<%= entidade.substring(0,1).toLowerCase() + entidade.substring(1) %>"/>

<div class="form-group">
	<label class="${cssClassLabel}" for="${atributo}">
		<spring:message code="${entidadeUpper}.${atributo}.label" /><c:if test="${required eq 'true'}"><b class="required-field"> *</b></c:if>
	</label>
	<div class="${cssClassComponente}">
		<c:if test="${required eq 'true'}">
			<form:input type="password" class="form-control" path="${atributo}" id="${atributo}" maxlength="${maxLength}" readonly="${readOnly}" required="true" />
		</c:if>
		<c:if test="${required ne 'true'}">
			<form:input type="password" class="form-control" path="${atributo}" id="${atributo}" maxlength="${maxLength}" readonly="${readOnly}" />
		</c:if>
	</div>
</div>
