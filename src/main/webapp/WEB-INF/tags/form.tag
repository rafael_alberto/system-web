<%@ tag description="form" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="entidade" required="true" %>
<%@ attribute name="metodo" required="true" %>
<%@ attribute name="upload" %>

<c:set var="entidadeUpper" value="<%= entidade.substring(0,1).toUpperCase() + entidade.substring(1) %>"/>
<c:set var="entidadeLower" value="<%= entidade.substring(0,1).toLowerCase() + entidade.substring(1) %>"/>

<section class="content-header">
	<h1 class="title">
		<spring:message code="${entidadeUpper}.form.label" />
	</h1>
</section>
<section class="content">
		<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title" style="color: gray;">
				<i class="fa fa-edit"></i> &nbsp;<spring:message code="global.cadastro.label"/>
			</h3>
		</div>
		<div class="required-div">
			<p></p>
			<b>(*)</b> <spring:message code="global.preenchimento.obrigatorio"/><p></p>
		</div>
		<c:if test="${upload ne 'true'}">
			<form:form id="form${entidadeUpper}" class="form-horizontal" method="post" modelAttribute="model" action="${metodo}">
				<jsp:doBody />		
			</form:form>
		</c:if>
		<c:if test="${upload eq 'true'}">
			<form:form id="form${entidadeUpper}" class="form-horizontal" enctype="multipart/form-data" method="post" modelAttribute="model" action="${metodo}">
				<jsp:doBody />		
			</form:form>
		</c:if>
		
		</div>
</section>

<script type='text/javascript'>
	var requiredFields = $('.required-field').length;
	if(requiredFields === 0){
		$('.required-div').hide();
	}
</script>
