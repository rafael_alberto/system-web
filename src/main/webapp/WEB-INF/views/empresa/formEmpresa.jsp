<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="system" tagdir="/WEB-INF/tags" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><spring:message code="Empresa.page.title" /></title>
	</head>
	<body>
		<div>
			<system:form entidade="Empresa" metodo="salvar.do">
				<div class="box-body">
					<c:if test="${model.id ne null}">
						<system:input-text entidade="Empresa" atributo="id" cssClassLabel="col-sm-3 control-label required" cssClassComponente="col-sm-2" readOnly="true"/>
					</c:if>
				
					<system:input-text entidade="Empresa" atributo="nome" cssClassLabel="col-sm-3 control-label" cssClassComponente="col-sm-4" maxLength="50" required="true"/>	
				</div>
				<div class="box-footer">
					<div class="col-sm-9 col-sm-offset-3">
						<system:button-submit cssClass="btn btn-primary" mensagem="global.salvar.label" icone="fa fa-save" />
						<system:ahref url="${context}/empresa/index.do" cssClass="btn btn-primary" icone="fa fa-mail-reply" mensagem="global.voltar.label"></system:ahref>
					</div>
				</div>
			</system:form>
		</div>
		
	</body>
</html>
