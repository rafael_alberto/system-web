<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="system" tagdir="/WEB-INF/tags" %>
<html>
    <head>
        <title><spring:message code="Empresa.page.title"/></title>
    </head>
    <body>
    	<system:table titulo="Empresa.escolher.label" camposTabela="${camposTabela}" entidade="empresa" cssClass="table table-bordered table-striped dataTable" esconderNovo="true" />
    </body>
</html>
