<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title><spring:message code="global.acesso.negado.titulo.label"/></title>
    </head>
    <body>
    	<h2><spring:message code="global.acesso.negado.titulo.label"/></h2>
    	<h3><spring:message code="global.acesso.negado.descricao.label"/></h3>
	</body>
</html>