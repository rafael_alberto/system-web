<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<title><spring:message code="global.login.label" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/bootstrap.min.css">
		<link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/AdminLTE.min.css">
		<link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/font-awesome.min.css">

		<!--[if lt IE 9]>
        	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo"><a href="#"><b>System</b> Web</a></div>
			<div class="login-box-body">
				<p class="login-box-msg"><spring:message code="global.login.label"/></p>
					<form action="acessar.do" method="post">
						<div class="form-group has-feedback">
							<input type="email" class="form-control" id="email" name="email" placeholder="E-mail"> <span
								class="glyphicon glyphicon-envelope form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha"> <span
								class="glyphicon glyphicon-lock form-control-feedback"></span>
						</div>
						<div class="row">
							<div class="col-xs-8"></div>
							<div class="col-xs-4">
								<button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
							</div>
						</div>
					</form>
			</div>
		</div>

		<script type="text/javascript" src="${context}/layout/javascript/system/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="${context}/layout/bootstrap/adminLTE/js/bootstrap.min.js"></script>
	</body>
</html>
