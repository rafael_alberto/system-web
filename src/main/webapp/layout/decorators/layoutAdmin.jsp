<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>System Web</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/bootstrap.min.css">
        <link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/AdminLTE.min.css">
        <link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/skin-blue-light.min.css">
        
        <link rel="stylesheet" href="${context}/layout/dataTable/css/dataTables.bootstrap.css">
        
        <link rel="stylesheet" href="${context}/layout/bootstrap/adminLTE/css/font-awesome.min.css">
    	
    	<link rel="stylesheet" href="${context}/layout/css/system.css">
    
    	<link rel="stylesheet" href="${context}/layout/nprogress/nprogress.css">
    
    	<script type="text/javascript" src="${context}/layout/nprogress/nprogress.js"></script>
    
        <!--[if lt IE 9]>
        	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->
        
    </head>
    <body class="sidebar-mini skin-blue-light">
    	<script type="text/javascript" src="${context}/layout/javascript/system/jquery-2.1.4.min.js"></script>
    	
    	<script type="text/javascript">
            var context = "${context}";
            
            NProgress.start();
            
            $(window).load(function (){
            	NProgress.done();
            });
            
        </script>
    	
    	<script type="text/javascript" src="${context}/layout/bootstrap/adminLTE/js/bootstrap.min.js"></script>
    
    	<script type="text/javascript" src="${context}/layout/bootstrap/adminLTE/js/app.min.js"></script>
    	
    	<script type="text/javascript" src="${context}/layout/javascript/system/jquery.validate.min.js"></script>
		<script type="text/javascript" src="${context}/layout/javascript/system/additional-methods.min.js"></script>
		<script type="text/javascript" src="${context}/layout/javascript/system/messages_pt_BR.js"></script>	
		
		<script type="text/javascript" src="${context}/layout/javascript/system/shortcut.js"></script>
		
		<script type="text/javascript" src="${context}/layout/dataTable/js/jquery.dataTables.min.js"></script>
		
    	<div class="wrapper">	
			<jsp:include page="/layout/includes/menu-top.jsp" />
      		<jsp:include page="/layout/includes/menu-left.jsp" />
      
      		<div class="content-wrapper">
      			<div>
        		<c:if test="${msgSuccess ne null}">
            		<div class="alert alert-info fade-in">
                		<button type="button" class="close" data-dismiss="alert">×</button>
                		${msgSuccess}
            		</div>
        		</c:if>
        		<c:if test="${msgError ne null}">
            		<div class="alert alert-danger fade-in">
                		<button type="button" class="close" data-dismiss="alert">×</button>
                		${msgError}
            		</div>
        		</c:if>
    			</div>
       			<decorator:body />
      		</div>
    	</div>
  </body>
</html>