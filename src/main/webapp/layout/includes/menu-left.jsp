<aside class="main-sidebar">
	<section class="sidebar">
        <ul class="sidebar-menu">
        	<li class="header">ADMINISTRADOR</li>
        	<li class="active"><a href="${context}/usuario/index.do"><i class="fa fa-user"></i> <span>Usu�rios</span></a></li>
        	<li class="active"><a href="${context}/empresa/index.do"><i class="fa fa-building-o"></i> <span>Empresas</span></a></li>	
        </ul>
    </section>
</aside>