package br.com.systemweb.core.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.systemweb.core.dao.GenericDao;
import br.com.systemweb.core.enumeration.GenericEnum;
import br.com.systemweb.core.system.CondicaoPesquisaEnum;
import br.com.systemweb.core.system.DataTableParametroBusca;
import br.com.systemweb.core.system.Utilitarios;

@Transactional
public abstract class GenericDaoImpl<T, PK> implements GenericDao<T, PK> {

	private static final int COLUNA_ORDENACAO_VAZIA = -1;

	private static final int ZERO = 0;
	private static final int ONE = 1;
	private static final Integer TEN = 10;

	@Autowired
	private SessionFactory sessionFactory;

	private Session session;

	@SuppressWarnings("unchecked")
	private final Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
			.getGenericSuperclass()).getActualTypeArguments()[0];

	public GenericDaoImpl() {
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		this.session = sessionFactory.getCurrentSession();
		return this.session;
	}

	@Override
	@Transactional
	public void save(T entity) {
		getSession().saveOrUpdate(entity);
	}

	@Override
	@Transactional
	public void delete(T entity) {
		getSession().delete(entity);
	}

	@Override
	@Transactional
	public long count() {
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.setProjection(Projections.rowCount());
		return (Long.valueOf(criteria.uniqueResult().toString()));

	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<T> findAll(Order order) {
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.addOrder(order);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<T> findAll() {
		Criteria criteria = getSession().createCriteria(clazz);
		return criteria.list();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public T findById(PK id) {
		return (T) getSession().createCriteria(clazz)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<T> findAllById(PK id) {
		return getSession().createCriteria(clazz)
				.add(Restrictions.eq("id", id)).list();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public T findByPk(Object pk) {
		return (T) getSession().get(clazz, (Serializable) pk);
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(Criterion criterion) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (criterion == null) {
			return null;
		} else {
			criteria.add(criterion);
		}
		return criteria.list();
	}

	@Override
	@Transactional
	@SuppressWarnings({ "rawtypes" })
	public List selectGenericWithHQL(String hql, Map<String, Object> params) {
		Query query = getSession().createQuery(hql);
		if (params != null) {
			Set<String> str = params.keySet();

			for (String string : str) {
				Object value = params.get(string);
				if ((value instanceof Collection))
					query.setParameterList(string, (Collection) value);
				else {
					query.setParameter(string, value);
				}
			}
		}
		return query.list();
	}

	@Override
	@Transactional
	@SuppressWarnings({ "rawtypes" })
	public List selectGenericWithHQL(String hql, Map<String, Object> params,
			Integer startRow, Integer maxResult) {
		Query query = getSession().createQuery(hql);
		if (params != null) {
			Set<String> str = params.keySet();

			for (String string : str) {
				Object value = params.get(string);
				if ((value instanceof Collection))
					query.setParameterList(string, (Collection) value);
				else {
					query.setParameter(string, value);
				}
			}
		}
		query.setFirstResult(startRow);
		query.setMaxResults(maxResult);
		return query.list();
	}

	@Override
	@Transactional
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> selectWithHQL(String hql, Map<String, Object> params) {
		Query query = getSession().createQuery(hql);
		if (params != null) {
			Set<String> str = params.keySet();

			for (String string : str) {
				Object value = params.get(string);
				if ((value instanceof Collection))
					query.setParameterList(string, (Collection) value);
				else {
					query.setParameter(string, value);
				}
			}
		}
		return query.list();
	}

	@Override
	@Transactional()
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> selectWithHQL(String hql, Map<String, Object> params,
			Integer maxResult) {
		Query query = getSession().createQuery(hql);
		if (params != null) {
			Set<String> str = params.keySet();

			for (String string : str) {
				Object value = params.get(string);
				if ((value instanceof Collection))
					query.setParameterList(string, (Collection) value);
				else {
					query.setParameter(string, value);
				}
			}
		}
		query.setMaxResults(maxResult);
		return query.list();
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public T selectFirstWithHQL(Session session, String hql, Map<String, Object> params) {
		Query query = getSession().createQuery(hql);
		if (params != null) {
			Set<String> str = params.keySet();

			for (String string : str) {
				Object value = params.get(string);
				if ((value instanceof Collection))
					query.setParameterList(string, (Collection) value);
				else {
					query.setParameter(string, value);
				}
			}
		}
		
		T result = null;
		List<T> resultList = (List<T>) query.list();
		if (resultList != null && !resultList.isEmpty()) {
			result = resultList.get(0);
		}
		return result;
	}
	
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<T> findByCriteriaWithOrder(List<Criterion> criterion,
			Order order) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (criterion == null) {
			return null;
		}
		List<Criterion> list = criterion;
		for (Criterion c : list) {
			criteria.add(c);
		}
		if (order != null) {
			criteria.addOrder(order);
		}
		return criteria.list();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<T> findByCriteriaWithOrder(Criterion criterion, Order order) {
		Criteria criteria = getSession().createCriteria(clazz);

		if (criterion == null) {
			return null;
		}
		criteria.add(criterion);
		if (order != null) {
			criteria.addOrder(order);
		}

		return criteria.list();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public T findByCriteriaFirstResult(Criterion criterion) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (criterion == null) {
			return null;
		}
		criteria.add(criterion);
		return (T) criteria.uniqueResult();

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> select(String[][] columns, Integer startRow,
			Integer maxResults, Integer sortColumnIndex, String sortDirection) {
		startRow = (startRow != null ? startRow : ZERO);
		maxResults = (maxResults != null ? maxResults : TEN);
		String hql = createHQLQuery(columns, sortColumnIndex, sortDirection);
		Query query = getSession().createQuery(hql);
		query.setFirstResult(startRow);
		query.setMaxResults(maxResults);
		return query.list();
	}

	@Override
	public Long count(String[][] columns, Integer sortColumnIndex,
			String sortDirection) {
		String hql = createHQLQuery(columns, sortColumnIndex, sortDirection);
		Query query = getSession().createQuery(hql);
		return (long) query.list().size();
	}

	private String createHQLQuery(String[][] columns, Integer sortColumnIndex,
			String sortDirection) {
		StringBuilder builder = new StringBuilder();
		builder.append(" FROM ").append(clazz.getSimpleName()).append(" m ");
		if (sortColumnIndex == null
				|| sortColumnIndex == COLUNA_ORDENACAO_VAZIA) {
			sortColumnIndex = ZERO;
		}
		builder.append(" ORDER BY ");
		for (int i = 0; i < columns.length; i++) {
			if (i == sortColumnIndex) {
				builder.append(columns[i]);
			}
		}
		if (sortDirection == null) {
			sortDirection = "asc";
		}
		builder.append(" ").append(sortDirection);

		return builder.toString();
	}

	@Override
	public void flush() {
		getSession().flush();
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> select(String[] columns, List<DataTableParametroBusca> listaParametros, Integer startRow,
			Integer maxResults, Integer sortColumnIndex, String sortDirection) {
		startRow = (startRow != null ? startRow : ZERO);
		maxResults = (maxResults != null ? maxResults : TEN);
		String hql = createHQLQuery(columns, listaParametros, sortColumnIndex, sortDirection);
		Query query = getSession().createQuery(hql);
		query.setFirstResult(startRow);
		query.setMaxResults(maxResults);

		Map<String, Object> params = new HashMap<String, Object>();
		if (!listaParametros.isEmpty()) {

			for (int indice = 0; indice < listaParametros.size(); indice++) {
				String parametro = listaParametros.get(indice).getAtributo();
				String valor = listaParametros.get(indice).getValor();
				params.put(parametro.replace(".", ""),
						converterCampo(parametro, valor));
			}

			for (String key : params.keySet()) {
				Object value = params.get(key);
				if (value instanceof Collection) {
					query.setParameterList(key, (Collection) value);
				} else {
					query.setParameter(key, value);
				}
			}
		}
		return query.list();
	}
	
	@Override
	@SuppressWarnings({ "rawtypes" })
	public Long count(String[] columns, List<DataTableParametroBusca> listaParametros) {
		String hql = createHQLQueryCount(columns, listaParametros);
		Query query = getSession().createQuery(hql);

		Map<String, Object> params = new HashMap<String, Object>();
		if (!listaParametros.isEmpty()) {

			for (int indice = 0; indice < listaParametros.size(); indice++) {
				String parametro = listaParametros.get(indice).getAtributo();
				String valor = listaParametros.get(indice).getValor();
				
				params.put(parametro.replace(".", ""), converterCampo(parametro, valor));
			}

			for (String key : params.keySet()) {
				Object value = params.get(key);
				if (value instanceof Collection) {
					query.setParameterList(key, (Collection) value);
				} else {
					query.setParameter(key, value);
				}
			}
		}

		if (query.list() != null) {
			return (Long) query.list().get(0);
		} else {
			return new Long(0);
		}
	}
	
	private String createHQLQuery(String[] columns, List<DataTableParametroBusca> listaParametros,Integer sortColumnIndex,
			String sortDirection) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT ");
		for(int indice = 0; indice < columns.length; indice ++){
			builder.append(columns[indice]);
			if(indice < columns.length - 1){
				builder.append(",");
			}
		}
		
		builder.append(" FROM ").append(clazz.getSimpleName()).append(" m ");

		createWhere(listaParametros, builder);

		if (sortColumnIndex == null
				|| sortColumnIndex == COLUNA_ORDENACAO_VAZIA
				|| sortColumnIndex >= columns.length) {
			sortColumnIndex = ZERO;
		}
		builder.append(" ORDER BY ");
		for (int i = 0; i < columns.length; i++) {
			if (i == sortColumnIndex) {
				builder.append(columns[i]);
				break;
			}
		}
		if (sortDirection == null) {
			sortDirection = "ASC";
		}
		builder.append(" ").append(sortDirection);

		return builder.toString();
	}
	
	private void createWhere(List<DataTableParametroBusca> listaParametros, StringBuilder builder) {
		if (!listaParametros.isEmpty()) {
			builder.append(" WHERE ");
			for (int indice = 0; indice < listaParametros.size(); indice++) {
				try{
				CondicaoPesquisaEnum condicao = listaParametros.get(indice).getCondicao();
				if (condicao.equals(CondicaoPesquisaEnum.CONTEM)) {
					if(indice > 0){
						builder.append(" AND ");
					}
					builder.append("UPPER( str( ")
					.append(listaParametros.get(indice).getAtributo())
					.append(")) ").append(condicao.getDescricao()).append(" :")
					.append(listaParametros.get(indice).getAtributo());
				} else {
					if(indice > 0){
						builder.append(" AND ");
					}
					builder.append(listaParametros.get(indice).getAtributo())
					.append(" ").append(condicao.getDescricao()).append(" :")
					.append(listaParametros.get(indice).getAtributo().replace(".", ""));
				}
				}catch(Exception e){
					
				}
			}
		}
	}
	
	private String createHQLQueryCount(String[] columns, List<DataTableParametroBusca> listaParametros) {
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT COUNT(").append(columns[0])
				.append(") FROM ").append(clazz.getSimpleName()).append(" m ");

		createWhere(listaParametros, builder);
		return builder.toString();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object converterCampo(String parametro, String valor) {
		try {
			String fieldName[] = parametro.split("\\.");
			Field field = null;
			
			if(fieldName.length == ONE){
				field = clazz.getDeclaredField(parametro);
			}else{
				field = clazz.getDeclaredField(fieldName[ZERO]);
				field = Class.forName(field.getType().toString().replace("class ", "")).getDeclaredField(fieldName[ONE]);
			}
			
			if(GenericEnum.class.isAssignableFrom(field.getType())){
				Class classEnum = Class.forName(field.getType().getName());
				return Enum.valueOf(classEnum, valor);
			}else{
				String tipo = field.getType().getSimpleName();
				if ("String".equals(tipo)) {
					return "%" + valor.toUpperCase() + "%";
				} else if ("Long".equals(tipo)) {
					return Long.valueOf(valor);
				} else if ("Integer".equals(tipo)) {
					return Integer.valueOf(valor);
				} else if ("Double".equals(tipo)) {
					return Double.valueOf(valor);
				} else if ("BigDecimal".equals(tipo)) {
					return BigDecimal.valueOf(Double.valueOf(valor));
				} else if ("Date".equals(tipo)) {
					return Utilitarios.converterStringEmDate(valor);
				} else {
					return "%" + valor.toUpperCase() + "%";
				}
			}
		} catch (Exception e) {
			return null;
		}
	}
}
