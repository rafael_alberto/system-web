package br.com.systemweb.core.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

import br.com.systemweb.core.system.DataTableParametroBusca;

@SuppressWarnings("rawtypes")
public interface GenericDao<T, PK> {

	void save(T entity);

	void delete(T entity);

	T findById(PK id);

	T findByPk(Object pk);

	List<T> findAll();

	List<T> findAll(Order order);

	T findByCriteriaFirstResult(Criterion criterion);

	List<T> findByCriteriaWithOrder(List<Criterion> criterion, Order order);

	List<T> findByCriteriaWithOrder(Criterion criterion, Order order);

	List<T> findByCriteria(Criterion criterion);

	List<T> findAllById(PK id);

	long count();

	List selectGenericWithHQL(String hql, Map<String, Object> params);

	List selectGenericWithHQL(String hql, Map<String, Object> params,
			Integer startRow, Integer maxResult);

	List<T> selectWithHQL(String hql, Map<String, Object> params);

	List<T> selectWithHQL(String hql, Map<String, Object> params,
			Integer maxResult);

	T selectFirstWithHQL(Session session, String hql, Map<String, Object> params);
	
	List<Object[]> select(String[][] columns, Integer startRow,
			Integer maxResults, Integer sortColumnIndex, String sortDirection);

	Long count(String[][] nomeCamposDataTable, Integer indiceColunaOrdenacao,
			String direcaoOrdenacao);

	void flush();
	
	public List<Object[]> select(String[] columns, List<DataTableParametroBusca> listaParametros, Integer startRow,
			Integer maxResults, Integer sortColumnIndex, String sortDirection);
	
	public Long count(String[] columns, List<DataTableParametroBusca> listaParametros);
}
