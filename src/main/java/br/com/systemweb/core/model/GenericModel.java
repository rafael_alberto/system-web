package br.com.systemweb.core.model;

import java.io.Serializable;

import javax.persistence.Transient;

public abstract class GenericModel implements Serializable {

	private static final long serialVersionUID = -7176129626605396465L;

	@Transient
	private boolean update = true;
	
	public abstract String getIdentificadorToView();
	
	public boolean isUpdate() {
		return update;
	}
	
	public void setUpdate(boolean update) {
		this.update = update;
	}
}
