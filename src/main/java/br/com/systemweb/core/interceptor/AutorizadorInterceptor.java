package br.com.systemweb.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.systemweb.admin.enumeration.PerfilUsuarioEnum;
import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.core.annotation.AcessoAnonimoAnnotation;
import br.com.systemweb.core.annotation.PermissaoAnnotation;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

	private static final String USUARIO_LOGADO = "usuarioLogado";
	private static final String LOGIN_DO = "/login.do";
	private static final String ACESSO_NEGADO_DO = "/acessoNegado.do";
	
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		AcessoAnonimoAnnotation acessoAnonimoAnnotation = (AcessoAnonimoAnnotation) getAnnotationFromHandler(handlerMethod, AcessoAnonimoAnnotation.class);
		if(acessoAnonimoAnnotation != null){
			return true;
		}
		
		PermissaoAnnotation permissaoAnnotation = (PermissaoAnnotation) getAnnotationFromHandler(handlerMethod, PermissaoAnnotation.class);
		if(permissaoAnnotation != null){
			Usuario usuarioLogado = getUsuarioLogado(request);
			if(!isUsuarioPermitido(usuarioLogado)){
				response.sendRedirect(request.getContextPath() + ACESSO_NEGADO_DO);
	    		return false;
			}
		}
		
		if(getUsuarioLogado(request) != null){
			return true;
		}
		
		response.sendRedirect(request.getContextPath() + LOGIN_DO);
		return false;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object getAnnotationFromHandler(HandlerMethod handlerMethod, Class clazz) {
		Object annotation = handlerMethod.getMethod().getAnnotation(clazz);
		if(annotation == null){
			annotation = handlerMethod.getBean().getClass().getAnnotation(clazz);
		}
		return annotation;
	}
	
	protected Usuario getUsuarioLogado(HttpServletRequest request){
		Usuario usuario = (Usuario) request.getSession().getAttribute(USUARIO_LOGADO);
		return usuario;
	}
	
	protected boolean isUsuarioPermitido(Usuario usuarioLogado) {
		return usuarioLogado != null ? usuarioLogado.getPerfil().equals(PerfilUsuarioEnum.ADM) : false;
	}
}
