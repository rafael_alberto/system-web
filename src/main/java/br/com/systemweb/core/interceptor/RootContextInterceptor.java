package br.com.systemweb.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

public class RootContextInterceptor implements HandlerInterceptor {

	private static final String CONTEXT = "context";

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) {
		if (modelAndView != null
				&& !(modelAndView.getView() instanceof RedirectView)
				&& (modelAndView.getViewName() == null || !modelAndView
						.getViewName().startsWith("redirect"))) {
			modelAndView.addObject(CONTEXT, request.getContextPath());
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
	}
}