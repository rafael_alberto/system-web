package br.com.systemweb.core.system;

import br.com.systemweb.core.enumeration.GenericEnum;

public enum CondicaoPesquisaEnum implements GenericEnum<String> {
	
	CONTEM("LIKE"), 
	IGUAL("="), 
	MAIOR (">"), 
	MENOR ("<"), 
	MAIOR_OU_IGUAL (">="), 
	MENOR_OU_IGUAL ("<=");
	
	private String descricao;
	
	private CondicaoPesquisaEnum(String descricao){
		this.descricao = descricao;
	}

	@Override
	public String getCodigo() {
		return this.descricao;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}
}
