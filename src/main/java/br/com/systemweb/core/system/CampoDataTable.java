package br.com.systemweb.core.system;

import lombok.Setter;

import lombok.Getter;

@Getter
@Setter
public class CampoDataTable {

	private String atributo;
	private Integer tamanho;
	private TipoAtributoEnum tipoAtributo = TipoAtributoEnum.TEXTO;
	
	public CampoDataTable(String atributo, String valorPadrao) {
		this.atributo = atributo;
	}
	
	public CampoDataTable(String atributo, Integer tamanho) {
		this.atributo = atributo;
		this.tamanho = tamanho;
	}
	
	public CampoDataTable(String atributo, TipoAtributoEnum tipoAtributo, Integer tamanho) {
		this.atributo = atributo;
		this.tipoAtributo = tipoAtributo;
		this.tamanho = tamanho;
	}
}
