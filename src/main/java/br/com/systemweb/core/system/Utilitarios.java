package br.com.systemweb.core.system;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.LocalDate;

public class Utilitarios {

	public static Locale localePtBr = new Locale("pt", "BR");
	
	private static final String FORMATO_DATA = "dd/MM/yyyy";
	
	public static Date converterStringEmDate(String data) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);
		try {
			return dateFormat.parse(data);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String converterDateEmString(Date date) {
		if (date != null) {
			return new LocalDate(date).toString(FORMATO_DATA);
		} else {
			return null;
		}
	}

	public static String abreviacao(String texto, int tamanho) {
		StringBuilder retorno = new StringBuilder();
		if (tamanho < texto.length()) {
			String textoReduzido = texto.substring(0, tamanho);
			for (int i = textoReduzido.length() - 1; i > 0; i--) {
				if (textoReduzido.charAt(i) == ' '
						|| textoReduzido.charAt(i) == 13
						|| textoReduzido.charAt(i) == 10) {
					retorno.append(textoReduzido.substring(0, i)).append("...");
					break;
				}
			}
		} else {
			retorno.append(texto);
		}
		return retorno.toString();
	}
	
	public static String lowerCaseFirstWord(String str) {
		String conteudo = str.substring(1, str.length());
		String upperCase = str.substring(0, 1).toLowerCase();
		return upperCase + conteudo;
	}
}
