package br.com.systemweb.core.system;

import java.lang.reflect.Field;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.builder.EqualsBuilder;

public class SystemEqualsBuilder {

	public boolean isEquals(Object s1, Object s2) {
		if (s1 == s2) {
			return true;
		} else if (s1 == null || s2 == null) {
			return false;
		} else if (!s1.getClass().equals(s2.getClass())) {
			return false;
		}
		EqualsBuilder eb = new EqualsBuilder();
		Class<?> clazz = s1.getClass();
		while (!clazz.equals(Object.class)) {
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				try {
					if (field.isAnnotationPresent(ManyToOne.class) || field.isAnnotationPresent(ManyToMany.class)
							|| field.isAnnotationPresent(OneToMany.class) || field.isAnnotationPresent(OneToOne.class)
							|| field.isAnnotationPresent(Transient.class)) {
						continue;
					}
					eb.append(PropertyUtils.getProperty(s1, field.getName()),
							PropertyUtils.getProperty(s2, field.getName()));
				} catch (Exception e) {
					continue;
				}
			}
			clazz = clazz.getSuperclass();
		}
		return eb.isEquals();
	}
}
