package br.com.systemweb.core.system;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class CriarTabela {

	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		configuration.configure();

		SchemaExport schemaExport = new SchemaExport(configuration);
		schemaExport.create(true, false);
	}

}
