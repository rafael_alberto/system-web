package br.com.systemweb.core.system;

public class GenericException extends RuntimeException {

	private static final long serialVersionUID = 3498913189227814079L;

	private String field;

	private String messageKey;

	private String[] arguments;

	public GenericException(String messageKey) {
		this.messageKey = messageKey;
	}

	public GenericException(String field, String messageKey) {
		this.messageKey = messageKey;
		this.field = field;
	}

	public GenericException(String messageKey, String[] arguments) {
		this.messageKey = messageKey;
		this.arguments = arguments.clone();
	}

	public String getMessageKey() {
		return messageKey;
	}

	public String getField() {
		return field;
	}

	public String[] getArguments() {
		return arguments;
	}

	public String getFormattedArguments() {
		String formattedArguments = null;
		if (arguments != null) {
			StringBuilder builder = new StringBuilder();
			boolean first = true;
			for (String argument : arguments) {
				if (!first) {
					builder.append(", ");
				}
				builder.append(argument);
				first = false;
			}
			formattedArguments = builder.toString();
		}
		return formattedArguments;
	}
}
