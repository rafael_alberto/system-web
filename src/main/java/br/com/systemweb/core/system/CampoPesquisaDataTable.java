package br.com.systemweb.core.system;

import lombok.Setter;

import lombok.Getter;

@Getter
@Setter
public class CampoPesquisaDataTable {

	private String atributo;
	private boolean visivel = true;
	private String valorPadrao;
	private Object[] valoresPesquisa;
	private TipoAtributoEnum tipoAtributo = TipoAtributoEnum.TEXTO;
	private CondicaoPesquisaEnum condicao = CondicaoPesquisaEnum.IGUAL;
	
	public CampoPesquisaDataTable(String atributo, String valorPadrao) {
		this.atributo = atributo;
		this.valorPadrao = valorPadrao;
	}
	
	public CampoPesquisaDataTable(String atributo, TipoAtributoEnum tipoAtributo, String valorPadrao, Object[] valoresPesquisa, 
			CondicaoPesquisaEnum condicao) {
		this.atributo = atributo;
		this.tipoAtributo = tipoAtributo;
		this.valorPadrao = valorPadrao;
		this.valoresPesquisa = valoresPesquisa;
		this.condicao = condicao;
	}
	
	public CampoPesquisaDataTable(String atributo, TipoAtributoEnum tipoAtributo, boolean visivel, String valorPadrao, 
			Object[] valoresPesquisa, CondicaoPesquisaEnum condicao) {
		this.atributo = atributo;
		this.tipoAtributo = tipoAtributo;
		this.visivel = visivel;
		this.valorPadrao = valorPadrao;
		this.valoresPesquisa = valoresPesquisa;
		this.condicao = condicao;
	}
}
