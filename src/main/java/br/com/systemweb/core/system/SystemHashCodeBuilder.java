package br.com.systemweb.core.system;

import java.lang.reflect.Field;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class SystemHashCodeBuilder {

	public int toHashCode(Object s1) {
		HashCodeBuilder hcb = new HashCodeBuilder();
		Class<?> clazz = s1.getClass();
		while (!clazz.equals(Object.class)) {
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				try {
					if (field.isAnnotationPresent(ManyToOne.class) || field.isAnnotationPresent(ManyToMany.class) || 
						field.isAnnotationPresent(OneToMany.class) || field.isAnnotationPresent(OneToOne.class) ||
						field.isAnnotationPresent(Transient.class)) {
						continue;
					}
					hcb.append(PropertyUtils.getProperty(s1, field.getName()));
				} catch (Exception e) {
					continue;
				}
			}
			clazz = clazz.getSuperclass();
		}
		return hcb.toHashCode();
	}
}
