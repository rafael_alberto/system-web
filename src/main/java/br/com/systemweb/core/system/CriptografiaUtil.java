package br.com.systemweb.core.system;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CriptografiaUtil {
	
	private final static int ITERATION_NUMBER = 1000;
	
	public static String getHashSalt(){
		SecureRandom random;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
			byte[] byteSalt = new byte[8];
			random.nextBytes(byteSalt);
			 
			StringBuilder hexSalt = new StringBuilder();
			for(byte b : byteSalt) {
			  hexSalt.append(String.format("%02X", 0xFF & b));
			}
			
			return hexSalt.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getHashSenha(String salt, String senha){
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.reset();
			messageDigest.update(salt.getBytes());
			 
			byte byteHash[] = messageDigest.digest(salt.concat(senha).getBytes("UTF-8"));
			 
			for(int indice = 0; indice < ITERATION_NUMBER; indice ++) {
			  messageDigest.reset();
			  byteHash = messageDigest.digest(byteHash);
			}
			 
			StringBuilder hexHash = new StringBuilder();
			for(byte b : byteHash) {
			  hexHash.append(String.format("%02X", 0xFF & b));
			}
			 
			return hexHash.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
