package br.com.systemweb.core.system;

import br.com.systemweb.admin.model.Usuario;

public final class GenericContext {

	private static final ThreadLocal<Usuario> usuarioHolder = new ThreadLocal<Usuario>();
	
	private GenericContext() {
		
	}

	public static void setUsuario(Usuario usuario) {
		usuarioHolder.set(usuario);
	}

	public static Usuario getUsuario() {
		return (Usuario) usuarioHolder.get();
	}
	
	public static void removerUsuario() {
		usuarioHolder.remove();
	}
}
