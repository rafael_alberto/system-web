package br.com.systemweb.core.system;

public class DataTableParametroBusca {

	private String atributo;
	private String valor;
	private CondicaoPesquisaEnum condicao = CondicaoPesquisaEnum.CONTEM;
	
	public String getAtributo() {
		return atributo;
	}
	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public CondicaoPesquisaEnum getCondicao() {
		return condicao;
	}
	public void setCondicao(CondicaoPesquisaEnum condicao) {
		this.condicao = condicao;
	}
}
