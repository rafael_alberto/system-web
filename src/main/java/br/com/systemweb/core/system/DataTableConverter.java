package br.com.systemweb.core.system;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import br.com.systemweb.core.enumeration.GenericEnum;

public class DataTableConverter {

	private static final int ZERO = 0;
	private static final int ONE = 1;

	public static void converterCamposDataTable(List<Object[]> listaResultado, String[] atributosDataTable, 
			Class<?> clazz, ReloadableResourceBundleMessageSource messageSource){
		for(Object[] object : listaResultado){
			for(int indice = ZERO; indice < atributosDataTable.length; indice ++){
				try {
					if(atributosDataTable[indice] != null){
						String fieldName[] = atributosDataTable[indice].split("\\.");
						Field field = null;
						
						if(fieldName.length == ONE){
							field = clazz.getDeclaredField(atributosDataTable[indice]);
						}else{
							field = clazz.getDeclaredField(fieldName[ZERO]);
							field = Class.forName(field.getType().toString().replace("class ", "")).getDeclaredField(fieldName[ONE]);
						}
						
						if(GenericEnum.class.isAssignableFrom(field.getType())){
							GenericEnum<?> enums = ((GenericEnum<?>) object[indice]);
							object[indice] = messageSource.getMessage(enums.getDescricao() , null, Utilitarios.localePtBr);
						}else if(Date.class.isAssignableFrom(field.getType())){
							object[indice] = Utilitarios.converterDateEmString((Date) object[indice]);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
