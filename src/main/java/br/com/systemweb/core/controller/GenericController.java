package br.com.systemweb.core.controller;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.com.systemweb.core.model.GenericModel;
import br.com.systemweb.core.service.GenericService;
import br.com.systemweb.core.system.CampoDataTable;
import br.com.systemweb.core.system.CampoPesquisaDataTable;
import br.com.systemweb.core.system.DataTableResponse;
import br.com.systemweb.core.system.GenericException;
import br.com.systemweb.core.system.Utilitarios;


public abstract class GenericController<T extends GenericModel, PK> {
	
	protected static final String MODEL = "model";
	
	protected static final String MSG_ERROR = "msgError";
	protected static final String MSG_SUCCESS = "msgSuccess";
	
	protected static final String INDEX_PREFIX = "/index";
	protected static final String FORM_PREFIX = "/form";
	
	protected static final String CAMPOS_TABELA = "camposTabela";
	protected static final String CAMPOS_PESQUISA_TABELA = "camposPesquisaTabela";
	
	@SuppressWarnings("unchecked")
	private final Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	@SuppressWarnings("unchecked")
	private final Class<PK> pkClazz = (Class<PK>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	
	@Autowired
    private Validator validator;

	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;

	@RequestMapping(value = "/index.do", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(getPastaComViews() + INDEX_PREFIX + clazz.getSimpleName());
        modelAndView.addObject(CAMPOS_TABELA, getCamposDataTable(request));
        modelAndView.addObject(CAMPOS_PESQUISA_TABELA, getCamposPesquisaDataTable(request));
        adicionarParametrosIndex(modelAndView, request);
        return modelAndView;
    }
	
	@RequestMapping(value = "/novo.do", method = RequestMethod.GET)
    public ModelAndView novo(HttpServletRequest request, @ModelAttribute(value = MODEL) T entidade) throws InstantiationException, IllegalAccessException {
		T model  = entidade;
        ModelAndView modelAndView = new ModelAndView(getPastaComViews() + FORM_PREFIX + clazz.getSimpleName());
        if (model == null) {
        	model = clazz.newInstance();
        }
        modelAndView.addObject(MODEL, model);
        adicionarParametrosNovo(modelAndView, request);
        return modelAndView;
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editar.do", method = RequestMethod.GET)
    public ModelAndView editar(@ModelAttribute(value = MODEL) T model, @RequestParam(value = "id") String id, HttpServletRequest request) {
        PK pk = (PK) getPk(id);
        T entidade = null;
        
        if(!model.isUpdate()){
        	entidade = model;
        }else{
        	entidade = getService().buscarPorId(pk);
        }
        
        if (entidade != null) {
            ModelAndView modelAndView = new ModelAndView(getPastaComViews() + FORM_PREFIX + clazz.getSimpleName());
            modelAndView.addObject(MODEL, entidade);
            adicionarParametrosEditar(pk, modelAndView, request);
            return modelAndView;
        } else {
            ModelAndView modelAndView = index(request);
            modelAndView.addObject(MSG_ERROR, getDescricao(clazz.getSimpleName() + ".nao.encontrado"));
            return modelAndView;
        }
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/visualizar.do", method = RequestMethod.GET)
    public ModelAndView visualizar(@RequestParam(value = "id") String id, HttpServletRequest request, HttpServletResponse response) {
        PK pk = (PK) getPk(id);
        GenericModel entidade = getService().buscarPorId(pk);
        if (entidade != null) {
            ModelAndView modelAndView = new ModelAndView(getPastaComViews() + FORM_PREFIX + clazz.getSimpleName());
            modelAndView.addObject(MODEL, entidade);
            adicionarParametrosEditar(pk, modelAndView, request);
            return modelAndView;
        } else {
            ModelAndView modelAndView = index(request);
            modelAndView.addObject(MSG_ERROR, getDescricao(clazz.getSimpleName() + ".nao.encontrado"));
            return modelAndView;
        }
    }

	@RequestMapping(value = "/salvar.do", method = RequestMethod.POST)
    public RedirectView salvar(@ModelAttribute(value = MODEL) T entidade, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		antesDeSalvar(entidade);
		RedirectView redirectView = criarRedirectView(entidade, redirectAttributes);
    	adicionarParametrosSalvar(entidade, redirectAttributes, request);
        try {
            validator.validate(entidade, result);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult." + MODEL, result);
            if (!result.hasErrors()) {
            	getService().salvar(entidade);
            	redirectView = new RedirectView("index.do", true);
            	redirectAttributes.addFlashAttribute(MSG_SUCCESS, getDescricao(clazz.getSimpleName() +  ".salvo.com.sucesso"));
            }else{
            	entidade.setUpdate(false);
            	redirectAttributes.addFlashAttribute(MSG_ERROR, getDescricao(result.getFieldError().getDefaultMessage()));
            	
            }
        } catch (GenericException e) {
        	entidade.setUpdate(false);
        	redirectAttributes.addFlashAttribute(MSG_ERROR, getDescricao(e.getMessageKey()));
        }
        return redirectView;
    }

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/excluir.do", method = RequestMethod.GET)
    public RedirectView excluir(@RequestParam(value = "id") String id,HttpServletRequest request, RedirectAttributes redirectAttributes) {
    	try {
            PK pk = (PK) getPk(id);
    		getService().excluir(pk);
    		redirectAttributes.addFlashAttribute(MSG_SUCCESS, getDescricao(clazz.getSimpleName() + ".excluido.com.sucesso"));
    	} catch (GenericException e) {
    		redirectAttributes.addFlashAttribute(MSG_ERROR, getDescricao(e.getMessageKey()));
    	}
    	return new RedirectView("index.do", true);
    }

	@ResponseBody
	@RequestMapping(value = "/loadDataAjax.do", method = RequestMethod.GET, produces="application/json")
	public DataTableResponse loadAjaxData(
			@RequestParam(value="sEcho") String sEcho, 
			@RequestParam(value="iDisplayStart") int startRow, 
			@RequestParam(value="iDisplayLength") int maxResults,
			@RequestParam(value="iSortCol_0", required=false) Integer sortColumnIndex,
			@RequestParam(value="sSortDir_0", required=false) String sortDirection,
			HttpServletRequest request) {
		try {
			return getService().buscarDataTableResponse(getCamposDataTable(request), getCamposPesquisaDataTable(request), sEcho, startRow, maxResults, sortColumnIndex, sortDirection, request);
		} catch (Exception e) {
			return criarDataTableResponseVazia(sEcho);
		}
	}

	protected abstract List<CampoDataTable> getCamposDataTable(HttpServletRequest request);
	
	protected abstract List<CampoPesquisaDataTable> getCamposPesquisaDataTable(HttpServletRequest request);
	
	protected abstract GenericService<T, PK> getService();
	
	protected void adicionarParametrosIndex(ModelAndView modelAndView, HttpServletRequest request) { }
	
	protected void adicionarParametrosNovo(ModelAndView modelAndView, HttpServletRequest request) { }
	
	protected void adicionarParametrosEditar(PK id, ModelAndView modelAndView, HttpServletRequest request) { }
	
	protected void adicionarParametrosSalvar(T entidade, RedirectAttributes redirectAttributes, HttpServletRequest request) { }
	
	protected void antesDeSalvar(T entidade) { }

	protected String getPastaComViews() {
		return nomeLowerCase();
	}
	
	protected String getDescricao(String codigo) {
		return messageSource.getMessage(codigo, null, Utilitarios.localePtBr);
	}
	
	protected String nomeLowerCase() {
		return Utilitarios.lowerCaseFirstWord(clazz.getSimpleName());
	}

	protected final Object getPk(String idStr) {
		if (Long.class.isAssignableFrom(pkClazz)) {
			return Long.parseLong(idStr);
		} else if (Integer.class.isAssignableFrom(pkClazz)) {
			return Integer.parseInt(idStr);
		} else if (String.class.isAssignableFrom(pkClazz)) {
			return idStr;
		}
		return null;
	}

	protected RedirectView criarRedirectView(T entidade,
			RedirectAttributes redirectAttributes) {
		String redirectUrl = entidade.getIdentificadorToView() == null ? "novo.do" : "editar.do?id=" + entidade.getIdentificadorToView();
		RedirectView redirectView = new RedirectView(redirectUrl , true);
		redirectAttributes.addFlashAttribute(MODEL, entidade);
		return redirectView;
	}
	
	private DataTableResponse criarDataTableResponseVazia(String sEcho) {
		DataTableResponse dataTableResponse = new DataTableResponse();
		dataTableResponse.setsEcho(sEcho);
		dataTableResponse.setiTotalDisplayRecords(0L);
		dataTableResponse.setiTotalRecords(0L);
		dataTableResponse.setAaData(new ArrayList<Object[]>());
		return dataTableResponse;
	}
}