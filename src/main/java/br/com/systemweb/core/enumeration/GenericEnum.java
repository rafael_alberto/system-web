package br.com.systemweb.core.enumeration;

public abstract interface GenericEnum<T> {

	public abstract T getCodigo();
	public abstract String getDescricao();
}
