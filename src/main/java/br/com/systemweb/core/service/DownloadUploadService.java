package br.com.systemweb.core.service;

import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

public interface DownloadUploadService {

	void download(String caminhoArquivo, String nomeArquivo, HttpServletResponse response);

	void upload(InputStream input, String caminhoArquivo, String nomeArquivo);

	void excluirArquivo(String caminhoArquivo, String nomeArquivo);

	String getCaminhoUploadAnexo();

	String getCaminhoUploadImagem();
}
