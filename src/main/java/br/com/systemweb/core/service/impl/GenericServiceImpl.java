package br.com.systemweb.core.service.impl;

import java.lang.reflect.ParameterizedType;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import br.com.systemweb.core.dao.GenericDao;
import br.com.systemweb.core.service.GenericService;
import br.com.systemweb.core.system.CampoDataTable;
import br.com.systemweb.core.system.CampoPesquisaDataTable;
import br.com.systemweb.core.system.DataTableConverter;
import br.com.systemweb.core.system.DataTableParametroBusca;
import br.com.systemweb.core.system.DataTableResponse;
import br.com.systemweb.core.system.TipoAtributoEnum;

public abstract class GenericServiceImpl<T, PK> implements GenericService<T, PK> {

	private static final String PREFIX_CUSTOM_FILTER = "customFilter_";
	private static final String VAZIO = "";
	
	protected abstract GenericDao<T, PK> getDao();
	
	@SuppressWarnings("unchecked")
	private final Class<T> clazz = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	
	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;
	
	@Override
	public DataTableResponse buscarDataTableResponse(
			List<CampoDataTable> camposDataTable, List<CampoPesquisaDataTable> camposPesquisas, String sEcho, Integer startRow,
			Integer maxResults, Integer sortColumnIndex, String sortDirection, HttpServletRequest request) {
		
		List<DataTableParametroBusca> listaParametros = new ArrayList<DataTableParametroBusca>();
		
		String[] atributosDataTable = getAtributosEBuscaDataTable(camposDataTable, camposPesquisas, listaParametros, request);
		
		List<Object[]> listaResultado = getDao().select(atributosDataTable, listaParametros, startRow, maxResults, sortColumnIndex, sortDirection);
		
		DataTableConverter.converterCamposDataTable(listaResultado, atributosDataTable, clazz, messageSource);
		
		Long count = getDao().count(atributosDataTable, listaParametros);
		
		DataTableResponse dataTableResponse = new DataTableResponse();
		dataTableResponse.setAaData(listaResultado);
		dataTableResponse.setsEcho(sEcho);
		dataTableResponse.setiTotalDisplayRecords(count);
		dataTableResponse.setiTotalRecords(count);
		return dataTableResponse;
	}
	
	private String[] getAtributosEBuscaDataTable(List<CampoDataTable> camposDataTable, List<CampoPesquisaDataTable> camposPesquisas, List<DataTableParametroBusca> listaParametros, HttpServletRequest request) {
		String[] atributos = new String[camposDataTable.size()];
		for(CampoDataTable campoDataTable : camposDataTable){
			if(campoDataTable.getTipoAtributo().equals(TipoAtributoEnum.TEXTO)){
				atributos[camposDataTable.indexOf(campoDataTable)] = campoDataTable.getAtributo();
			}
		}
		
		adicionarParametroBusca(camposPesquisas, listaParametros, request);
		
		return atributos;
	}
	
	private void adicionarParametroBusca(List<CampoPesquisaDataTable> camposPesquisas, List<DataTableParametroBusca> listaParametros, HttpServletRequest request) {		
			if(!camposPesquisas.isEmpty()){
				for(CampoPesquisaDataTable campoPesquisa : camposPesquisas){
					String valor = campoPesquisa.getValorPadrao() == null || campoPesquisa.getValorPadrao().equals("") 
							? request.getParameter(PREFIX_CUSTOM_FILTER.concat(campoPesquisa.getAtributo())) : campoPesquisa.getValorPadrao();

					if (valor != null) {
						byte[] bytes = valor.getBytes(StandardCharsets.ISO_8859_1);
						valor = new String(bytes, StandardCharsets.UTF_8);
						
						if(!valor.equals(VAZIO)){
							DataTableParametroBusca parametroBusca = new DataTableParametroBusca();
							parametroBusca.setAtributo(campoPesquisa.getAtributo());
							parametroBusca.setValor(valor);
							parametroBusca.setCondicao(campoPesquisa.getCondicao());
							listaParametros.add(parametroBusca);
						}
					}
				}
			}
	}
}
