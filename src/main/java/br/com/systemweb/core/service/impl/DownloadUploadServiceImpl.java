package br.com.systemweb.core.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import br.com.systemweb.core.service.DownloadUploadService;
import br.com.systemweb.core.system.GenericException;


@Service
public class DownloadUploadServiceImpl implements DownloadUploadService {

	public static final String CAMINHO_UPLOAD_IMAGEM = "/home/rafael/";
	public static final String CAMINHO_UPLOAD_ANEXO = "/home/rafael/upload/";

	@Override
	public void download(String caminhoArquivo, String nomeArquivo,
			HttpServletResponse response) {

		File anexo = new File(caminhoArquivo.concat(nomeArquivo));

		String[] arquivo = nomeArquivo.split("\\.");
		String extensao = arquivo[1];

		if (extensao.equalsIgnoreCase("TXT")) {
			response.setContentType("application/txt");
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ nomeArquivo + "\"");
		} else {
			response.setContentType("application/pdf");
		}

		try {
			URL url = anexo.toURI().toURL();
			BufferedInputStream input;

			input = new BufferedInputStream(url.openStream(), 4 * 1024);

			OutputStream output = response.getOutputStream();
			byte[] buffer = new byte[4 * 1024];
			int size = 0;
			while ((size = input.read(buffer, 0, buffer.length)) != -1) {
				output.write(buffer, 0, size);
			}
			output.close();
			input.close();
		} catch (FileNotFoundException e) {
			throw new GenericException("global.erro.arquivo.nao.encontrado");
		} catch (Exception e) {
			throw new GenericException("global.erro.ao.executar.download");
		}
	}

	@Override
	public void upload(InputStream input, String caminhoArquivo,
			String nomeArquivo) {
		try {
			InputStream inputStream = input;
			String nomeOriginal = nomeArquivo;
			String caminhoUpload = caminhoArquivo.concat(nomeOriginal);
			OutputStream outputStream = new FileOutputStream(caminhoUpload);
			int readBytes = 0;
			byte[] buffer = new byte[10000];

			while ((readBytes = inputStream.read(buffer, 0, 10000)) != -1) {
				outputStream.write(buffer, 0, readBytes);
			}
			outputStream.close();
			inputStream.close();
		} catch (FileNotFoundException ex) {
			throw new GenericException(
					"global.erro.diretorio.nao.encontrado");
		} catch (IOException ex) {
			throw new GenericException("global.erro.ao.executar.upload");
		}
	}

	@Override
	public void excluirArquivo(String caminhoArquivo, String nomeArquivo) {
		try {
			File file = new File(caminhoArquivo.concat(nomeArquivo));
			if (file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			throw new ServiceException("global.erro.ao.excluir.arquivo");
		}
	}

	@Override
	public String getCaminhoUploadAnexo() {
		return CAMINHO_UPLOAD_ANEXO;
	}

	@Override
	public String getCaminhoUploadImagem() {
		return CAMINHO_UPLOAD_IMAGEM;
	}

}
