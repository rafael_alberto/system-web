package br.com.systemweb.core.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.systemweb.core.system.CampoDataTable;
import br.com.systemweb.core.system.CampoPesquisaDataTable;
import br.com.systemweb.core.system.DataTableResponse;

public interface GenericService<T, PK> {

	void salvar(T entidade);

	void excluir(PK id);

	T buscarPorId(PK id);
	
	List<T> listarTodos();
	
	DataTableResponse buscarDataTableResponse(List<CampoDataTable> camposDataTable, List<CampoPesquisaDataTable> camposPesquisas, String sEcho, Integer startRow, 
			Integer maxResults, Integer sortColumnIndex, String sortDirection, HttpServletRequest request);
}
