package br.com.systemweb.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.systemweb.admin.model.Empresa;
import br.com.systemweb.admin.service.EmpresaService;
import br.com.systemweb.core.annotation.PermissaoAnnotation;
import br.com.systemweb.core.controller.GenericController;
import br.com.systemweb.core.service.GenericService;
import br.com.systemweb.core.system.CampoDataTable;
import br.com.systemweb.core.system.CampoPesquisaDataTable;
import br.com.systemweb.core.system.CondicaoPesquisaEnum;
import br.com.systemweb.core.system.TipoAtributoEnum;

@Controller
@PermissaoAnnotation
@RequestMapping("/empresa")
public class EmpresaController extends GenericController<Empresa, Long> {

	@Autowired
	private EmpresaService empresaService;
	
	@Override
	protected List<CampoDataTable> getCamposDataTable(HttpServletRequest request) {
		List<CampoDataTable> camposDataTable = new ArrayList<CampoDataTable>();
		camposDataTable.add(new CampoDataTable("id", 10));
		camposDataTable.add(new CampoDataTable("nome", 25));
		camposDataTable.add(new CampoDataTable("editar", TipoAtributoEnum.BOTAO, 5));
		camposDataTable.add(new CampoDataTable("excluir", TipoAtributoEnum.BOTAO, 5));
		return camposDataTable;
	}

	@Override
	protected List<CampoPesquisaDataTable> getCamposPesquisaDataTable(HttpServletRequest request) {
		List<CampoPesquisaDataTable> camposPesquisaDataTable = new ArrayList<CampoPesquisaDataTable>();
		camposPesquisaDataTable.add(new CampoPesquisaDataTable("nome", TipoAtributoEnum.TEXTO, null, null, CondicaoPesquisaEnum.CONTEM));
		return camposPesquisaDataTable;
	}
	
	@Override
	protected GenericService<Empresa, Long> getService() {
		return empresaService;
	}
	
	@RequestMapping(value = "/escolher.do", method = RequestMethod.GET)
    public ModelAndView escolher(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(getPastaComViews() + "/escolher" + "Empresa");
        modelAndView.addObject(CAMPOS_TABELA, getCamposDataTableEscolherEmitente(request));
        modelAndView.addObject(CAMPOS_PESQUISA_TABELA, getCamposPesquisaDataTable(request));
        adicionarParametrosIndex(modelAndView, request);
        return modelAndView;
    }
	
	private List<CampoDataTable> getCamposDataTableEscolherEmitente(HttpServletRequest request) {
		List<CampoDataTable> camposDataTable = new ArrayList<CampoDataTable>();
		camposDataTable.add(new CampoDataTable("id", 10));
		camposDataTable.add(new CampoDataTable("nome", 25));
		return camposDataTable;
	}
}
