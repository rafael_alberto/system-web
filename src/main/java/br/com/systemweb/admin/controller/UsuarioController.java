package br.com.systemweb.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.systemweb.admin.enumeration.PerfilUsuarioEnum;
import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.admin.service.UsuarioService;
import br.com.systemweb.core.annotation.PermissaoAnnotation;
import br.com.systemweb.core.controller.GenericController;
import br.com.systemweb.core.service.GenericService;
import br.com.systemweb.core.system.CampoDataTable;
import br.com.systemweb.core.system.CampoPesquisaDataTable;
import br.com.systemweb.core.system.CondicaoPesquisaEnum;
import br.com.systemweb.core.system.TipoAtributoEnum;

@Controller
@PermissaoAnnotation
@RequestMapping("/usuario")
public class UsuarioController extends GenericController<Usuario, Long> {

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	protected List<CampoDataTable> getCamposDataTable(HttpServletRequest request) {
		List<CampoDataTable> camposDataTable = new ArrayList<CampoDataTable>();
		camposDataTable.add(new CampoDataTable("id", 10));
		camposDataTable.add(new CampoDataTable("nome", 25));
		camposDataTable.add(new CampoDataTable("email", 30));
		camposDataTable.add(new CampoDataTable("perfil", 10));
		camposDataTable.add(new CampoDataTable("dataCadastro", 15));
		camposDataTable.add(new CampoDataTable("editar", TipoAtributoEnum.BOTAO, 5));
		camposDataTable.add(new CampoDataTable("excluir", TipoAtributoEnum.BOTAO, 5));
		return camposDataTable;
	}

	@Override
	protected List<CampoPesquisaDataTable> getCamposPesquisaDataTable(HttpServletRequest request) {
		List<CampoPesquisaDataTable> camposPesquisaDataTable = new ArrayList<CampoPesquisaDataTable>();
		camposPesquisaDataTable.add(new CampoPesquisaDataTable("nome", TipoAtributoEnum.TEXTO, null, null, CondicaoPesquisaEnum.CONTEM));
		camposPesquisaDataTable.add(new CampoPesquisaDataTable("email", TipoAtributoEnum.TEXTO, null, null, CondicaoPesquisaEnum.CONTEM));
		camposPesquisaDataTable.add(new CampoPesquisaDataTable("perfil", TipoAtributoEnum.LISTA, null, PerfilUsuarioEnum.values(), CondicaoPesquisaEnum.IGUAL));
		camposPesquisaDataTable.add(new CampoPesquisaDataTable("dataCadastro", TipoAtributoEnum.DATA, null , null, CondicaoPesquisaEnum.MAIOR_OU_IGUAL));
		return camposPesquisaDataTable;
	}
	
	@Override
	protected void adicionarParametrosNovo(ModelAndView modelAndView,
			HttpServletRequest request) {
		modelAndView.addObject("listaPerfilUsuario", PerfilUsuarioEnum.values());
		super.adicionarParametrosNovo(modelAndView, request);
	}
	
	@Override
	protected void adicionarParametrosEditar(Long id,
			ModelAndView modelAndView, HttpServletRequest request) {
		modelAndView.addObject("listaPerfilUsuario", PerfilUsuarioEnum.values());
		super.adicionarParametrosEditar(id, modelAndView, request);
	}
	
	@Override
	protected GenericService<Usuario, Long> getService() {
		return usuarioService;
	}
	
	
}
