package br.com.systemweb.admin.controller;

import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.admin.service.UsuarioService;
import br.com.systemweb.core.annotation.AcessoAnonimoAnnotation;
import br.com.systemweb.core.system.GenericContext;
import br.com.systemweb.core.system.Utilitarios;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LoginController {

	private static final String FORM_LOGIN = "login/formLogin";	
	private static final String FORM_HOME = "home";
	private static final String FORM_ACESSO_NEGADO = "acessoNegado";
	
	private static final String LOGIN_DO = "login.do";
	private static final String HOME_DO = "home.do";
	private static final String ESCOLHER_EMPRESA_DO = "escolherEmpresa.do";
	private static final String EMPRESA_INDEX_DO = "empresa/escolher.do";
	
	private static final String USUARIO_LOGADO = "usuarioLogado";
	private static final String MSG_ERROR = "msgError";
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;
	
	@AcessoAnonimoAnnotation
	@RequestMapping("/start.do")
	public RedirectView start(final HttpSession httpSession){
		httpSession.removeAttribute(USUARIO_LOGADO);
		GenericContext.removerUsuario();
		return new RedirectView(LOGIN_DO, true);
	}
	
	@AcessoAnonimoAnnotation
	@RequestMapping("/" + LOGIN_DO)
	public ModelAndView login(){
		return new ModelAndView(FORM_LOGIN);
	}
	
	@AcessoAnonimoAnnotation
	@RequestMapping("/acessar.do")
	public RedirectView acessar(HttpServletRequest request, final String email, 
			final String senha, RedirectAttributes redirectAttributes){
		
		HttpSession httpSession = request.getSession();
		Usuario usuarioLogado = usuarioService.buscarPorEmailESenha(email, senha);	
		if (usuarioLogado != null) {
			httpSession.setAttribute(USUARIO_LOGADO, usuarioLogado);
			GenericContext.setUsuario(usuarioLogado);
			return new RedirectView(HOME_DO, true);
		} else {
			logout(httpSession);
			redirectAttributes.addFlashAttribute(MSG_ERROR, 
					messageSource.getMessage("Usuario.login.senha.invalido" , null, Utilitarios.localePtBr));
		}
		return new RedirectView(LOGIN_DO, true);
	}
	
	@RequestMapping("/" + ESCOLHER_EMPRESA_DO)
	public RedirectView escolherEmpresa(){
		return new RedirectView(EMPRESA_INDEX_DO, true);
	}
	
	@RequestMapping(value = "/" + HOME_DO)
	public ModelAndView home() {
		return new ModelAndView(FORM_HOME);
	}
	
	@RequestMapping("/logout.do")
	public RedirectView logout(final HttpSession httpSession){
		return start(httpSession);
	}
	
	@RequestMapping(value = "acessoNegado.do")
	public ModelAndView acessoNegado() {
		return new ModelAndView(FORM_ACESSO_NEGADO);
	}
}
