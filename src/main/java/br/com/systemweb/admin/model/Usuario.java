package br.com.systemweb.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;

import br.com.systemweb.admin.enumeration.PerfilUsuarioEnum;
import br.com.systemweb.core.model.GenericModel;
import br.com.systemweb.core.system.SystemEqualsBuilder;
import br.com.systemweb.core.system.SystemHashCodeBuilder;
import br.com.systemweb.core.system.Utilitarios;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "usuarios")
public class Usuario extends GenericModel implements Serializable {

	private static final long serialVersionUID = -4165994197997435624L;

	private static final String SEQUENCE_NAME = "seq_usuarios";
	
	@Id
	@GeneratedValue(generator = SEQUENCE_NAME)
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "nome", nullable = false, length = 70)
	private String nome;

	@NotBlank(message = "Usuario.email.deve.ser.informado")
	@Column(name = "email", nullable = false, length = 50)
	private String email;

	@Column(name = "salt", nullable = false, length = 20)
	private String salt;

	@Column(name = "senha", nullable = false, length = 70)
	private String senha;

	@Enumerated(EnumType.STRING)
	@Column(name = "perfil", nullable = false, length = 3)
	private PerfilUsuarioEnum perfil;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_cadastro", nullable = false)
	private Date dataCadastro;
	
	public String getDataCadastro() {
		return Utilitarios.converterDateEmString(dataCadastro);
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = Utilitarios.converterStringEmDate(dataCadastro);
	}

	@Override
	public boolean equals(Object obj) {
		return new SystemEqualsBuilder().isEquals(this, obj);
	}

	public int hashCode() {
		return new SystemHashCodeBuilder().toHashCode(this);
	}

	@Override
	public String getIdentificadorToView() {
		return getId() != null ? getId().toString() : null;
	}
}