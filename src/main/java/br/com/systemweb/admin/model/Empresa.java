package br.com.systemweb.admin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.systemweb.core.model.GenericModel;
import br.com.systemweb.core.system.SystemEqualsBuilder;
import br.com.systemweb.core.system.SystemHashCodeBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "empresas")
public class Empresa extends GenericModel implements Serializable {

	private static final long serialVersionUID = 1695129853584242250L;

	private static final String SEQUENCE_NAME = "seq_empresas";
	
	@Id
	@GeneratedValue(generator = SEQUENCE_NAME)
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "nome", nullable = false, length = 70)
	private String nome;

	@Override
	public boolean equals(Object obj) {
		return new SystemEqualsBuilder().isEquals(this, obj);
	}

	public int hashCode() {
		return new SystemHashCodeBuilder().toHashCode(this);
	}

	@Override
	public String getIdentificadorToView() {
		return getId() != null ? getId().toString() : null;
	}
}