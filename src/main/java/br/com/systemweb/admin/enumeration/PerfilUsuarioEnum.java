package br.com.systemweb.admin.enumeration;

import br.com.systemweb.core.enumeration.GenericEnum;

public enum PerfilUsuarioEnum implements GenericEnum<String>{

	ADM ("Usuario.perfil.administrador.label"),
	USU ("Usuario.perfil.usuario.label");

	private String descricao;
	
	private PerfilUsuarioEnum(String descricao){
		this.descricao = descricao;
	}
	
	@Override
	public String getCodigo() {
		return this.descricao;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}
	
}
