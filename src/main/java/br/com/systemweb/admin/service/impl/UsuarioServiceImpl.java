package br.com.systemweb.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.systemweb.admin.dao.UsuarioDao;
import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.admin.service.UsuarioService;
import br.com.systemweb.core.dao.GenericDao;
import br.com.systemweb.core.service.impl.GenericServiceImpl;
import br.com.systemweb.core.system.CriptografiaUtil;

@Service
public class UsuarioServiceImpl extends GenericServiceImpl<Usuario, Long> implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	public void salvar(Usuario entidade) {
		entidade.setSalt(CriptografiaUtil.getHashSalt());
		entidade.setSenha(CriptografiaUtil.getHashSenha(entidade.getSalt(), entidade.getSenha()));
		usuarioDao.save(entidade);
	}

	@Override
	public void excluir(Long id) {
		Usuario usuario = buscarPorId(id);
		usuarioDao.delete(usuario);
	}

	@Override
	public Usuario buscarPorId(Long id) {
		return usuarioDao.findById(id);
	}

	@Override
	public List<Usuario> listarTodos() {
		return usuarioDao.findAll();
	}
	
	@Override
	public Usuario buscarPorEmailESenha(String email, String senha) {
		Usuario usuario = usuarioDao.buscarPorEmail(email);
		
		if(usuario != null){
			String hashSalt = usuario.getSalt();
			String senhaGerada = CriptografiaUtil.getHashSenha(hashSalt, senha);
			
			if(senhaGerada.equals(usuario.getSenha())){
				return usuario;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	protected GenericDao<Usuario, Long> getDao() {
		return usuarioDao;
	}
}
