package br.com.systemweb.admin.service;

import br.com.systemweb.admin.model.Empresa;
import br.com.systemweb.core.service.GenericService;

public interface EmpresaService extends GenericService<Empresa, Long> {
	
}
