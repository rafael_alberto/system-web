package br.com.systemweb.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.systemweb.admin.dao.EmpresaDao;
import br.com.systemweb.admin.model.Empresa;
import br.com.systemweb.admin.service.EmpresaService;
import br.com.systemweb.core.dao.GenericDao;
import br.com.systemweb.core.service.impl.GenericServiceImpl;

@Service
public class EmpresaServiceImpl extends GenericServiceImpl<Empresa, Long> implements EmpresaService {

	@Autowired
	private EmpresaDao empresaDao;

	@Override
	protected GenericDao<Empresa, Long> getDao() {
		return empresaDao;
	}

	@Override
	public void salvar(Empresa entidade) {
		empresaDao.save(entidade);
	}

	@Override
	public void excluir(Long id) {
		Empresa empresa = buscarPorId(id);
		empresaDao.delete(empresa);
	}

	@Override
	public Empresa buscarPorId(Long id) {
		return empresaDao.findById(id);
	}

	@Override
	public List<Empresa> listarTodos() {
		return empresaDao.findAll();
	}
}
