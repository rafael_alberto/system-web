package br.com.systemweb.admin.service;

import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.core.service.GenericService;

public interface UsuarioService extends GenericService<Usuario, Long> {

    Usuario buscarPorEmailESenha(String email, String senha);
}
