package br.com.systemweb.admin.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.systemweb.admin.dao.UsuarioDao;
import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.core.dao.impl.GenericDaoImpl;

@Repository
public class UsuarioDaoImpl extends GenericDaoImpl<Usuario, Long> implements UsuarioDao{

	private static final String EMAIL = "email";
	
	@Override
	public Usuario buscarPorEmail(String email) {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.eq(EMAIL, email));
		return (Usuario) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listarUsuariosWS() {
		return selectGenericWithHQL("SELECT nome, email FROM Usuario ORDER by nome ASC", null);
	}
}
