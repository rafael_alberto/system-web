package br.com.systemweb.admin.dao;

import br.com.systemweb.admin.model.Empresa;
import br.com.systemweb.core.dao.GenericDao;

public interface EmpresaDao extends GenericDao<Empresa, Long>{
	
}
