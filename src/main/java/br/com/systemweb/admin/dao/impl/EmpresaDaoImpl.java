package br.com.systemweb.admin.dao.impl;

import org.springframework.stereotype.Repository;

import br.com.systemweb.admin.dao.EmpresaDao;
import br.com.systemweb.admin.model.Empresa;
import br.com.systemweb.core.dao.impl.GenericDaoImpl;

@Repository
public class EmpresaDaoImpl extends GenericDaoImpl<Empresa, Long> implements EmpresaDao{
	
}
