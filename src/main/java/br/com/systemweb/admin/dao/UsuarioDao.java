package br.com.systemweb.admin.dao;

import java.util.List;

import br.com.systemweb.admin.model.Usuario;
import br.com.systemweb.core.dao.GenericDao;

public interface UsuarioDao extends GenericDao<Usuario, Long>{
	Usuario buscarPorEmail(String email);
	List<Object[]> listarUsuariosWS();
}
