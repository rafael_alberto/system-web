package br.com.systemweb.webservice.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.systemweb.core.annotation.AcessoAnonimoAnnotation;
import br.com.systemweb.webservice.model.UsuarioWS;
import br.com.systemweb.webservice.service.UsuarioWSService;

@AcessoAnonimoAnnotation
@Component
@Path("/ws/usuario")
public class UsuarioWSController {

	@Autowired
	private UsuarioWSService usuarioWSService;

	@GET
	@Path("/listarUsuarios")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response listarUsuarios() {
		UsuarioWS.Response response = new UsuarioWS.Response();
		response.addUsuarios(usuarioWSService.listarUsuariosWS());		
		return Response.status(Response.Status.OK).entity(response).build();
	}
}
