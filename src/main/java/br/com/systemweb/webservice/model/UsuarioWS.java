package br.com.systemweb.webservice.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@XmlRootElement(name = "usuario")
public class UsuarioWS {

	private String nome;

	private String email;

	public UsuarioWS(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}

	@XmlElement
	public String getNome() {
		return nome;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}

	@NoArgsConstructor
	@XmlRootElement(name = "retorno")
	public static class Response {

		private List<UsuarioWS> usuarios;

		public void addUsuarios(List<UsuarioWS> usuarios) {
			this.usuarios = usuarios;
		}

		@XmlElement
		public List<UsuarioWS> getUsuarios() {
			return usuarios;
		}
	}
}
