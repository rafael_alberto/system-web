package br.com.systemweb.webservice.service;

import java.util.List;

import br.com.systemweb.webservice.model.UsuarioWS;

public interface UsuarioWSService {

	List<UsuarioWS> listarUsuariosWS();
}
