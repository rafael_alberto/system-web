package br.com.systemweb.webservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.systemweb.admin.dao.UsuarioDao;
import br.com.systemweb.webservice.model.UsuarioWS;
import br.com.systemweb.webservice.service.UsuarioWSService;

@Service
public class UsuarioWSServiceImpl implements UsuarioWSService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	public List<UsuarioWS> listarUsuariosWS() {
		List<Object[]> usuarios = usuarioDao.listarUsuariosWS();
		List<UsuarioWS> usuariosWS = new ArrayList<UsuarioWS>();
		if(!usuarios.isEmpty()){
			for(Object[] object : usuarios){
				usuariosWS.add(new UsuarioWS(String.valueOf(object[0]), String.valueOf(object[1])));
			}
		}
		return usuariosWS;
	}
}
